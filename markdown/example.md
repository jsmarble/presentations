---
author: Benji Fisher
title: Example for Pandoc and Reveal.js
date: August 5, 2018
revealjs-url: 'reveal.js'
theme: isovera
css:
  - 'https://fonts.googleapis.com/css?family=Roboto+Slab:700'
---

## Nested slides

- Each slide begins with an `<h2>` element (`"## "` in markdown).
- Use an `<h1>` element (`"# "` in markdown) to group slides.

## Our slogan

> Design. Develop. Drupal.
