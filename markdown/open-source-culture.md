---
title-meta: Creating An Open-Source Culture In A Closed-Source Company
author-meta: Joshua Marble
output: revealjs::revealjs_presentation
revealjs-url: 'reveal.js'
theme: rsa
---
## Creating An<br />Open-Source Culture<br />In A Closed-Source Company
**Joshua Marble**  
*Software Architect, RSA*  

::: notes
Introduce yourself  
DevOpsDays KC in the Fall

Most don't work at open-source company  
Company may or may not be open-source friendly  
Bet some want to open-source some of your code  

Explain RSA history of rejecting open-source  

Talk focus on open-source in company that sells software  
Not necessarily open-sourcing paid product  
Creating open-source code that augments your paid product  
Infusing open-source culture into closed-source company
:::

# So you want to create an open-source project ...
::: notes
If you told someone at your company  
What's the likely response you're going to get?
:::

## You want to do what?
::: notes
In a closed-source company, don't expect a positive reaction.
:::

## *Giving away software can be hard.*
::: notes
Giving away software can be hard, especially for business people  
Conversations - realize due to misunderstanding  

Business people already have idea of how software business works  
Also have a lot of misconceptions about open-source
:::

# Breaking Down Misconceptions
::: notes
First thing, break down misconceptions  
Continue to break down misconceptions throughout the process  

More you do this - More people understand where you’re going  
Start infusing open-source culture  

Talk about misconceptions likely encounter  
:::

# Monetization

::: notes
Probably the first concerns you'll encounter are all about money.  
On surface, does not make sense financially
:::

## *How can I make money?*

::: notes
First question - business person mindset - how can I make money from this?  
Open-source - we can't charge for it.  
Why do this?

Direct correlation - product/revenue.  
Traditional software sales process - New feature, win sales, provide service, subscription revenue  
You’re proposing something that doesn’t fit this mindset.
:::

## *Won't this cost money?*

::: notes
Second question is almost same, but from a different perspective.  

Not only no revenue from this, but it will cost money to develop.  

Why would we invest in something that won't make us any money?
:::

## Money Talks
* Don't worry
* Embrace the questions

::: notes
These questions can be intimidating  
Don't be scared of talking about money.  

They're looking for answers, they just don't understand.  

Opportunity - show why you want to do this  
Explain how open-source can be advantage.
:::

# Competition
::: notes
Other main concern - competition.  
Can feel like risk to publish your code.  
Could feel like giving up advantage in the market.
:::

## Competitive Advantage
:::::::::::::: {.columns}
::: {.column width="45%"}
* Steal Code
* Code Exposure
:::
::: {.column width="55%"}
* Innovations
:::
::::::::::::::

::: notes
Business concerned about competitive advantage  

Concerned with things like ...  
Will our competitors steal our code?  
Maybe just use our innovations in their product.  
Will our code be criticized or will people see it's not secure?

These all seem reasonable - but think for long and you realize they’re unrealistic  
:::

## Steal Code
:::::::::::::: {.columns}
::: {.column width="45%"}
* Trade secrets
:::
::: {.column width="55%"}
* Competitive advantage
:::
::::::::::::::

::: notes
When think about stealing code  
Two categories of code secrets  
True trade secrets  
General competitive advantage
  
It's unlikely that your code will have true trade secrets embedded in it.  
However, you will want to keep that in mind and your legal team will probably ask about it.

Concern - losing competitive advantage.  
The code itself is likely not a competitive advantage.  
How the project creates value for your business is your competitive advantage.  
Don't lose that by making the code open-source.
:::

## Innovations
:::::::::::::: {.columns}
::: {.column width="45%"}
* Secret sauce
:::
::: {.column width="55%"}
* Patents
:::
::::::::::::::

::: notes
Put innovations out for all to see  
We'll pay all the development cost  
Competitors will get the rewards from our work

Unlikely that you have some secret sauce in your code that would magically make your competitors successful.  
If you truly have innovations, protect them the right way, using appropriate legal avenues.

If something is patentable, work with your legal team on it.  
Doesn't mean you can't still open-source  
Talk more later ...
:::

## Code Exposure
:::::::::::::: {.columns}
::: {.column width="45%"}
* Quality
:::
::: {.column width="55%"}
* Security
:::
::::::::::::::

::: notes
Exposing your code - feel risky

The most likely criticism would be code quality.  
Competitors won’t get a competitive advantage by disparaging your code.  
The good news is that can be fixed.  
Create good standards and start fixing things.  
You can log issues and accept merge requests, too.  
Easy way to get engagement with contributors.

The other criticism would be security.  
But really, you should welcome that.  
We all know "security through obscurity" is not a good practice  
Well-accepted - open-source software - generally more secure  
The more eyes the better
:::

# Organization

::: notes
Covered many misconceptions - business, competition.  
Convinced the business project has potential, next how to organize it.
:::

## Project Management
:::::::::::::: {.columns}
::: {.column width="45%"}
* Legal Concerns
:::
::: {.column width="55%"}
* Project Control
:::
::::::::::::::

::: notes
Need a plan for project management  
Critical part of project  
Two main parts to project management - legal, project control
:::

## Legal Concerns
:::::::::::::: {.columns}
::: {.column width="45%"}
* Legal Nightmare
* OSS License
:::
::: {.column width="55%"}
* Intellectual Property
:::
::::::::::::::
  
::: notes
Show stopper

Natural to avoid legal issues,  
On the mind of the business person.

First reaction - walking into a legal nightmare  

Many concerns when you start thinking about legal issues  
Two big ones - Intellectual property and licensing
:::

## Intellectual Property
:::::::::::::: {.columns}
::: {.column width="45%"}
* Copyright
* Trademarks
:::
::: {.column width="55%"}
* Patents
:::
::::::::::::::

::: notes
Biggest legal concern - intellectual property.

Open-source - don't you lose ability protect innovations?  
Truth is - Protecting software already complicated - doesn’t mean you can’t.  

Variety of licenses - make the code public - still provide legal protections.

Will contributors hold copyright? Bad for business.  
Most open-source licenses address this - include the copyright in the license.  
Could have separate contributor agreement, but may make project less friendly to contributors.

Anything patentable?  
Still can be protected  
Could dictate OSS license  
Involve legal team

Concern about using trademarks from the business - should be protected?
:::

## Project Control
:::::::::::::: {.columns}
::: {.column width="45%"}
* Lose Control
:::
::: {.column width="55%"}
* Forking
:::
::::::::::::::

::: notes
Concern about losing control - not realistic.  
Some people don't understand how open-source works.  
May believe anyone can push code into project.  
Reassure them - still have control of the project and ultimately decide what makes it into the code.

May encounter concern over forking  
Sure, someone could fork it and try to make a competing project, but isn’t that a good problem?  
Remember - project enhances value of paid product.  
That much passion for project - you’ve created a strong competitive advantage.  
:::

## Code Quality
:::::::::::::: {.columns}
::: {.column width="45%"}
* Code chaos
:::
::: {.column width="55%"}
* Standards
:::
::::::::::::::

::: notes
Accepting merge requests can be concerning.  
What if the code is messy or poorly written?  
Will the code follow my coding standards?  

Code quality can be controlled - contribution guidelines, merge requests, code reviews, etc.  
Nobody can force to accept code that doesn't meet standards.  
That's why important to document standards for project.
:::

# Open-Source Benefits
::: notes
Encountered a lot of misconceptions  
Time to counter those misconceptions  
Opportunity to show benefits
:::

# Monetization
::: notes
We already covered this - can't make money - right?  
Tempted to avoid topic - don't  
Opportunity here
:::

## Indirect Monetization
:::::::::::::: {.columns}
::: {.column width="45%"}
* Enablement
* Services
:::
::: {.column width="55%"}
* Entrenchment
* Culture
:::
::::::::::::::

::: notes
Pivot to "indirect monetization"  
Many ways to add value  
drive revenue  
increase customer retention  
create competitive advantages  
:::

## Enablement
:::::::::::::: {.columns}
::: {.column width="45%"}
* Integrations
* Automation
:::
::: {.column width="55%"}
* Plugins
* Custom Dev
:::
::::::::::::::

::: notes
Any enablement creates value

Integrations - enable customers and Prof Services team to create value

More integrate with other business-critical products,  
the more value and the more critical to customer

Plugins, automation - add value, meet business needs

Custom development - ultimate enablement  
Customers invest - an integral part of their business
:::

## Entrenchment
:::::::::::::: {.columns}
::: {.column width="45%"}
* Investment
* Capability
:::
::: {.column width="55%"}
* Processes
:::
::::::::::::::

::: notes
Invested in enablements  
Processes built around them  
Motivated to keep your product

Capabilities from enablements - competitive advantage  
Make product more enticing and competitive  
Entrench customers in your product and helps your business grow
:::

## Services
:::::::::::::: {.columns}
::: {.column width="45%"}
* Create Value
* Revenue
:::
::: {.column width="55%"}
* Capability
* Entrenchment
:::
::::::::::::::

::: notes
Enablement helps the Prof Services team to create value, too

Give Prof Services team more capability  
Makes them more marketable

These solutions are unique for every customer  
Recurring revenue stream

Custom services - Further entrench product while creating revenue
:::

## Culture
:::::::::::::: {.columns}
::: {.column width="45%"}
* Connected
* Contributing
:::
::: {.column width="55%"}
* Community
:::
::::::::::::::

::: notes
Not only product and service value  
You can gain a cultural advantage around your product, too  

Customers that feel connected and invested in a product are far less likely to move away from it  
They attribute more value to your product just because of the connections they have beyond the product itself

Customers that feel a shared community around a product will be a competitive advantage  
They will help each other, increase retention, and even recruit industry peers

Open source - opportunity customers to contribute to product - ownership
:::

## Competitive Advantage
* Indirect Revenue
* Market Motivators

::: notes
None directly create product sales revenue  
But they can indirectly create revenue through other channels  

Or they can be powerful motivators in the market and competitive landscape  
This leads to more sales and more revenue
:::

# Where Do I Start?
::: notes
You're convinced! Excited!  
Want to write some code!  
Where to start

That’s easy! - github or gitlab - create new project.  
Right?

No, you do not want to just go publish some code and start building software.  
You'll have enemies for your project faster than you can commit code.  
:::

## *Politics anyone?*
::: notes
Instead of building software - start building team of supporters.  
:::

# Building Support
::: notes
People in the company - can help you work through challenges  
Identify  
Role
:::

## Manager
:::::::::::::: {.columns}
::: {.column width="45%"}
* Support
* Recruiting
:::
::: {.column width="55%"}
* Time
:::
::::::::::::::

::: notes
The first person you need to get involved is your manager.

Manager should be biggest supporter  
You don't want to catch them off guard

You'll need to clear the use of your time

Can help you identify the other people you need recruit for the cause
:::

## Business Partner
:::::::::::::: {.columns}
::: {.column width="45%"}
* Customer Needs
* Rapport
:::
::: {.column width="55%"}
* Vision
* Fortitude
:::
::::::::::::::

::: notes
Next - business partner.  
Not your boss or even someone in your department.  
Credibility outside technical circles.

Who - understands needs of customers?  
This is probably someone who actually talks to customers.

Who - engage with the vision?  
You want someone that understands why you want to do this and can see the benefit of it.

Who - has rapport with other people?  
Cranky guy that is always complaining?  
You want someone that can influence others for your cause.

Who - has fortitude to push against resistence?  
Because you will encounter resistence.
:::

## Executive Sponsor
:::::::::::::: {.columns}
::: {.column width="45%"}
* Authority
* Vision
* Legal Insight
:::
::: {.column width="55%"}
* Influence
* Budget
:::
::::::::::::::

::: notes
Good - Have business partner - More than just a computer nerd behind this.  
Project must have merit.  
So -- time to find an executive sponsor.

Not to help sell idea - that's business partner  
Help navigate bureaucracy and legal hurdles that are coming.

That's why you need someone with authority and influence.  
Likely director or VP  
Manager or Business Partner can help bring them in  
Best to be up the chain from you

Understand the vision - or harder to get help 

Provide a budget for your project.

They can also get insight into the legal process.

Which brings us to ...
:::

# Legal 
::: notes
Likely nobody wants to work with lawyers  
But ... next slide
:::

## *A Necessary ~~Evil~~ Partner*
::: notes
They are a necessary ... partner in this process

Questioned later - how impacted intellectual property  
Definitely want to say you had approval from Legal.
:::

## Legal Questions
:::::::::::::: {.columns}
::: {.column width="45%"}
* Can we do this?
:::
::: {.column width="55%"}
* Required Processes
:::
::::::::::::::

::: notes
Help answer questions like ...

Can we do this?  
Any established path?  
Outright ban?

How?
Required processes?  
Formal application?  
Need approval?  
:::

## Intellectual Property
:::::::::::::: {.columns}
::: {.column width="45%"}
* Copyright
* Trademarks
:::
::: {.column width="55%"}
* Patents
:::
::::::::::::::

::: notes
Most licenses cover copyright  

Anything Patentable? - Publish code - public disclosure - wait to release  
Contributor patents - May need license with express patent grant included.

Allowed to use trademarks?  
Logos protected?
:::

## OSS License
:::::::::::::: {.columns}
::: {.column width="45%"}
* Permissive
:::
::: {.column width="55%"}
* Copyleft
:::
::::::::::::::

::: notes
Permissive licenses have no requirements for downstream licenses.  
Use your software without concern over their own licensing  
Examples are MIT, Apache 2.0, BSD.  
If your dependencies are permissive, you can also choose a permissive license.  
This gives the most freedom in using your code.

However, if you are using dependencies licensed under a strong copyleft license, such as GPL  
You will be required to adopt it as well.  
If your company is a closed-source product, it's highly unlikely you want a copyleft license.

This is why your legal team needs to be involved in license selection.
:::

## OSS License
:::::::::::::: {.columns}
::: {.column width="45%"}
* Project Goals
* MIT
* https://choosealicense.com/
:::
::: {.column width="55%"}
* Dependencies
* Apache 2.0
:::
::::::::::::::

::: notes
Help choose OSS License  
Which makes sense - project goals, dependencies?  
Help legal determine

MIT - short and sweet - copyright  
Apache 2.0 - more complex - includes express grant of patent rights  
choosealicense.com - still work with legal - helps understand differences
:::

# Create the Project
::: notes
Business on board  
Legal signed off  
Time to finally start
:::

## Project Site
:::::::::::::: {.columns}
::: {.column width="45%"}
* Hosting Platform
* Branding
* README 
:::
::: {.column width="55%"}
* Contact Info
* OSS License
* Artifacts
:::
::::::::::::::

::: notes
Hosting platform  
Gitlab and Github are two popular choices  
Company precedent for platform?

Contact info  
email address?  
forum?

Branding  
company branding?  
logo?

OSS License  
included in project

README  
what is this?  
why did you make it?  
how do I use it?  

Artifacts  
just code?  
publish to package manager?  
zip file?
:::

## Project Management
:::::::::::::: {.columns}
::: {.column width="45%"}
* Issue Tracking
* CI/CD Pipeline
:::
::: {.column width="55%"}
* Merge Requests
:::
::::::::::::::

::: notes
Issue tracking  
how do people report things?  
how to track your own ideas?  

Merge requests  
Who is responsible?  
What process to accept one?  
Who has to approve?  

CI/CD  
Unit tests  
Dependency scans  
Vulnerability scans  
Artifacts  
:::

## Community Interaction
* Contribution Guidelines
* Responsible Disclosure of Vulnerabilities
* Code of Conduct 
  * [www.contributor-covenant.org](https://www.contributor-covenant.org/)

::: notes
Contribution guidelines  
Strict format?  
Naming conventions?
How interact?

Part of interaction ...
Responsibile Disclosure of Vulnerabilities  
Provide a private method of communicating vulnerabilities  
Do not log them as a public issue

Code of Conduct  
A clear statement of inclusiveness.  
You don't have to create this yourself!  
Contributor Covenant: "be overt in our openness", "foster an atmosphere of kindness, cooperation, and understanding"
:::

# Results?
::: notes
We did all of these  
What change have we seen?
:::

## Culture Change
* Shift in Culture
* Acceptance of Open-source

::: notes
Shift in culture, more teams thinking about open-source  
Acceptance of open-source at the business level
:::

## Other Projects
* New open-source projects
* Business engagement

::: notes
See project teams writing code in open-source  
Business people engaging with open-source  
Talking about open-source like it's a feature now
:::

## What Can OSS Do For You?
::: notes
I hope this session has triggered some ideas for you.  
What could you make open-source?  
Who could you work with to infuse open-source culture?  
How could your company benefit from that open-source culture?
:::

# Thank You
::: notes
Thank you for your time this morning.  
I appreciate the opportunity to be here.  
Find me around later to discuss how you  
could create an open-source culture in your closed-source company
:::