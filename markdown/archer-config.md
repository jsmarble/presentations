---
title: Archer Config
title-meta: Archer Config
author-meta: Joshua Marble
output: revealjs::revealjs_presentation
revealjs-url: 'reveal.js'
theme: rsa 
---
# Current State

## Config Service
* Shared config
* Shared store
* WCF / .NET
* Coupled client library
* Deployment concerns

# Future State

## Microservice Config
* Independent
* Does not affect other services
* Store depending on need

# Getting There
## Config Design
* Key-value pairs
* Simple data
* Standardized format
* REST API
* Generic client
* Store flexibility
  * Consul KV
  * Redis

## Config Service Interpreter
* New microservice / Windows service
* Translate REST requests in new design to config service
* Allows on-prem to avoid a new data store

## Questions
* Shared config or propogation?
  * Keep different redis keys in sync?
  * How to sync settings across services?
* WCF on .NET Core?
